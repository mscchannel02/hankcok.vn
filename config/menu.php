<?php

return [
    'label'=>'Category',
    'route'=>'category.index',
    'item'=>[
        [
            'label'=>'List Category',
            'route'=>'category.index'
        ],
        [
            'label'=>'Create Category',
            'route'=>'category.create'
        ]

    ]
]

?>