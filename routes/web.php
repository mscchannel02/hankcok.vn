<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\HomeController;
use App\Http\Controllers\Web\UserController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\KhoachuyenmonController;
use App\Http\Controllers\Admin\KhoahocController;
use App\Http\Controllers\Admin\SinhvienController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('admin')->name('ad.')->group(function () {
    Route::get('login', [LoginController::class, 'index'])->name('login.index');
    Route::post('login', [LoginController::class, 'login'])->name('login');

    Route::middleware(['auth'])->group(function () {
        Route::get('/', function () {
            return view('admin.dashboard');
        })->name('dashboard');
    
        Route::resource('category', CategoryController::class)->except('show');
        Route::resource('blog', BlogController::class)->except('show');
        Route::resource('admin', AdminController::class)->except('show');

        Route::resource('sinhvien', SinhvienController::class)->except('show');
        Route::resource('khoahoc', KhoahocController::class)->except('show');
        Route::resource('khoachuyenmon', KhoachuyenmonController::class)->except('show');
    
        Route::get('logout', [LoginController::class, 'logout'])->name('logout');
        Route::get('search', [CategoryController::class, 'search'])->name('category.search');
    });

});


Route::name('w.')->namespace('Web')->group(function(){
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('shop', [HomeController::class, 'shop'])->name('shop');

    Route::get('login', [UserController::class, 'login'])->name('login');
    Route::post('login', [UserController::class, 'checklogin'])->name('get.login');

    Route::get('register', [UserController::class, 'register'])->name('register');
    Route::post('register', [UserController::class, 'getregister'])->name('get.register');


});





    


