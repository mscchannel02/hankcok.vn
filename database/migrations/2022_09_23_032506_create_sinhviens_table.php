<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSinhviensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sinhviens', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->tinyInteger('gender')->default(1)->comment('1:trai|2:gái');
            $table->date('date');
            $table->string('address');
            $table->unsignedBigInteger('makhoacm_id');
            $table->unsignedBigInteger('makhoahoc_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sinhviens');
    }
}
