<!-- Main Header -->
<header class="main-header header-style-one">

    <!-- Header Upper -->
    <div class="header-upper">
        <div class="auto-container">
            <div class="inner-container row">
                <!--Logo-->
                <div class="logo-box ">
                    <div class="logo"><a href="{{route('w.home')}}"><img src="{{ asset('assets/images/logo.png') }}" alt=""></a></div>
                </div>
                <div class="contact-info ">
                    <div class="single-info ">
                        <div class="icon"><span class="flaticon-null-1"></span></div>
                        <div class="text">587 E,  Greenville Avenue <br> California, TX 70240</div>
                    </div>
                    <div class="single-info ">
                        <div class="icon"><span class="flaticon-null-2"></span></div>
                        <h5><a href="tel:(1800) 456 7890">(1800) 456 7890</a> <br>Call us</h5>
                    </div>
                </div>

                <div class="login ">
                    <a class="btn btn-primary" href="{{ route('w.login') }}">Login</a>
                    <a class="btn btn-warning" href="{{ route('w.register') }}">register</a>
                </div>
            </div>
        </div>
    </div>
    <!--End Header Upper-->

    <!-- Main Menu -->
    <div class="main-menu">
        <div class="auto-container">
            <div class="inner-container">
                <span class="border-shape"></span>
                <!--Nav Box-->
                <div class="nav-outer">
                    <!--Mobile Navigation Toggler-->
                    <div class="mobile-nav-toggler"><img src="{{ asset('assets/images/icons/icon-bar.png') }}" alt=""></div>

                    <!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md navbar-light">
                        <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                            <ul class="navigation">
                                <li class="dropdown"><a href="{{route('w.home')}}">Home </a>
                                    <ul>
                                        <li><a href="{{route('w.home')}}">Home Default</a></li>
                                        <li><a href="index-2.html">Home Page 02</a></li>
                                        <li><a href="index-3.html">Home Page 03</a></li>
                                        <li><a href="index-4.html">Home Page 04</a></li>
                                        <li class="dropdown"><a href="{{route('w.home')}}">Home </a>
                                            <ul>
                                                <li><a href="{{route('w.home')}}">Home Default</a></li>
                                                <li><a href="index-2.html">Home Page 02</a></li>
                                                <li><a href="index-3.html">Home Page 03</a></li>
                                                <li><a href="index-4.html">Home Page 04</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="about.html">About Us</a>
                                    <ul>
                                        <li><a href="about.html">Our Introduction</a></li>
                                        <li><a href="farmers.html">Our Farmers</a></li>
                                        <li><a href="testimonials.html">Testimonials</a></li>
                                        <li><a href="gallery.html">Our Gallery</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="{{route('w.shop')}}">Products</a>
                                    <ul>
                                        <li><a href="{{route('w.shop')}}">Products</a></li>
                                        <li><a href="product-details.html">Product Details</a></li>
                                        <li><a href="cart.html">Cart</a></li>
                                        <li><a href="checkout.html">Checkout</a></li>
                                    </ul>
                                </li>
                                <li><a href="services.html">Services</a></li>
                                <li class="dropdown"><a href="blog.html">Blog</a>
                                    <ul>
                                        <li><a href="blog.html">Blog</a></li>
                                        <li><a href="blog-2.html">Blog 02 Column</a></li>
                                        <li><a href="blog-details.html">Blog Details</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="recipes.html">Recipes</a>
                                    <ul>
                                        <li><a href="recipes.html">Recipes</a></li>
                                        <li><a href="recipe-details.html">Recipe Details</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav>
                    <!-- Main Menu End-->
                    <button type="button" class="theme-btn search-toggler"><span class="stroke-gap-icon icon-Search"></span></button>                        
                </div>
            </div>
        </div>
    </div>

    <!-- Sticky Header  -->
    <div class="sticky-header main-menu">
        <div class="auto-container">
            <div class="inner-container">
                <div class="nav-outer">
                    <span class="border-shape"></span>
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <!--Keep This Empty / Menu will come through Javascript-->
                    </nav><!-- Main Menu End-->
                    <!-- Main Menu End-->
                    <button type="button" class="theme-btn search-toggler"><span class="stroke-gap-icon icon-Search"></span></button>
                </div>                        
            </div>
        </div>
    </div><!-- End Sticky Menu -->

    <!-- Mobile Menu  -->
    <div class="mobile-menu">
        <div class="menu-backdrop"></div>
        <div class="close-btn"><span class="icon flaticon-remove"></span></div>
        
        <nav class="menu-box">
            <div class="nav-logo"><a href="{{route('w.home')}}"><img src="{{ asset('assets/images/logo-three.png') }}" alt="" title=""></a></div>
            <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            <!--Social Links-->
            <div class="social-links">
                <ul class="clearfix">
                    <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                    <li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
                    <li><a href="#"><span class="fab fa-pinterest-p"></span></a></li>
                    <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                    <li><a href="#"><span class="fab fa-youtube"></span></a></li>
                </ul>
            </div>
        </nav>
    </div><!-- End Mobile Menu -->

    <div class="nav-overlay">
        <div class="cursor"></div>
        <div class="cursor-follower"></div>
    </div>
</header>
<!-- End Main Header -->