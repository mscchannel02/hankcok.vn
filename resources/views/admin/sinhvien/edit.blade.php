@extends('admin.layouts.master')

@section('content')
    @if ($errors->any())
        <p class="alert alert-danger">
            <span>Vui lòng kiểm tra lại các trường</span>
        </p>
    @endif

    <a class="btn btn-sm btn-success" href="{{ route('ad.sinhvien.index') }}">List Danh mục</a>
    <form method="post" action="{{ route('ad.sinhvien.update', $sinhvien) }}" class="text-white">
        @csrf
        @method('PUT')
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">name</label>
                <input type="text" class="form-control" id="name" placeholder="name" name="name" value="{{$sinhvien->name}}">
                @error('name')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            

            <div class="form-group col-md-6">
                <label for="address">address</label>
                <input type="text" class="form-control" id="address" placeholder="address" name="address" value="{{$sinhvien->address}}">
                @error('address')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="form-group">
            <label for="date">date</label>
            <input type="date" class="form-control" name="date" value="{{$sinhvien->date}}" id="date" placeholder="date">
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="gender">gender</label>
                <select name="gender" id="gender" class="form-control" >
                    <option value="1" selected>Nam</option>
                    <option value="0">Nữ</option>
                </select>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Sửa</button>
    </form>
@endsection
