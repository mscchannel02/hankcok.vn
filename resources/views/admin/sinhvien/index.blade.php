@extends('admin.layouts.master')

@section('content')
    <div class="row mb-3">
        <div class="col-md-6">
            <a class="btn btnsm btn btn-success"
                href="{{ route('ad.sinhvien.create') }}">Tạo mới</a>
        </div>
        <div class="col-md-6 text-right">
            <caption>
                <form class="w-100 d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                    <div class="input-group">
                        <input type="text" class="form-control small" placeholder="Search for..."
                            aria-label="Search" aria-describedby="basic-addon2" name="search">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">
                                <i class="fa fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </caption>
        </div>
    </div>
    <table class="table table-dark table-hover text-white">
        <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">Name</th>
                <th scope="col">Gender</th>
                <th scope="col">Ngày sinh</th>
                <th scope="col">Địa chỉ</th>
                <th scope="col">Khóa học</th>
                <th scope="col">Khoa chuyên môn</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($sinhviens as $key => $sinhvien)
                <tr>
                    {{-- <th scope="row">{{ $key + 1 }}</th> --}}
                    <th scope="row">{{ $sinhvien->id }}</th>
                    <td>{{ $sinhvien->name }}</td>
                    <td>
                        @if ($sinhvien->gender == 1)
                            <span class="btn-primary btn-sm">Nam</span>
                        @else
                            <span class="btn-warning btn-sm">Nữ</span>
                        @endif

                    </td>
                    <td>{{ $sinhvien->date }}</td>
                    <td>{{ $sinhvien->address }}</td>
                    <td>{{ $sinhvien->khoahoc->name }}</td>
                    <td>{{ $sinhvien->khoachuyenmon->name }}</td>
                    <td>
                        <a href="{{ route('ad.sinhvien.edit', $sinhvien) }}" class="btn btn-sm btn-success">
                            <i class="fa fa-edit"></i>
                        </a>

                        <form class="d-inline-block" action="{{ route('ad.sinhvien.destroy', $sinhvien) }}" method="post" onsubmit="return confirm('Bạn có chắc chắn muốn xóa ?')">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-sm btn-danger" type="submit">
                                <i class="fa fa-trash "></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>
    {{-- <form action="" method="post" id="formDelete">
        @csrf
        @method('DELETE')
    </form> --}}
    <hr>
    {{ $sinhviens->links() }}
@endsection


{{-- @push('js')
    <script>
        $('.btnDelete').click(function(e) {
            e.preventDefault();
            var _href = $(this).attr('href');
            // alert(href);
            $('#formDelete').attr('action', _href);
            
            if(confirm('Bạn có chắc muốn xóa không ?')) {
                $('#formDelete').submit();
            }
        });
    </script>
@endpush --}}

@stack('name')

@push('name')
    
@endpush
