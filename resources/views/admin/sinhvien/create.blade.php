@extends('admin.layouts.master')

@section('content')
    @if ($errors->any())
        <p class="alert alert-danger">
            <span>Vui lòng kiểm tra lại các trường</span>
        </p>
    @endif

    <a class="btn btn-sm btn-success" href="{{ route('ad.sinhvien.index') }}">List Danh sách</a>
    <form method="post" action="{{ route('ad.sinhvien.store') }}" class="text-white">
        @csrf
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">name</label>
                <input type="text" class="form-control" id="name" placeholder="name" name="name" value="{{old('name')}}">
                @error('name')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            

            <div class="form-group col-md-6">
                <label for="address">address</label>
                <input type="text" class="form-control" id="address" placeholder="address" name="address" value="{{old('address')}}">
                @error('address')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="form-group">
            <label for="date">date</label>
            <input type="date" class="form-control" name="date" value="{{old('date')}}" id="date" placeholder="date">
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="gender">gender</label>
                <select name="gender" id="gender" class="form-control">
                    <option value="1" selected>Nam</option>
                    <option value="0">Nữ</option>
                </select>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="makhoahoc_id">Khóa học<span class="required">*</span></label>
                <select name="makhoahoc_id" class="form-control" id="makhoahoc_id">
                    @foreach($khoahocs as $khoahoc)
                    <option value="{{ $khoahoc->id }}">{{ $khoahoc->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-md-6">
                <label for="makhoacm_id">Khoa chuyên môn<span class="required">*</span></label>
                <select name="makhoacm_id" class="form-control" id="makhoacm_id">
                    @foreach($khoachuyenmons as $khoachuyenmon)
                    <option value="{{ $khoachuyenmon->id }}">{{ $khoachuyenmon->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <button type="submit" class="btn btn-sm btn-primary">Thêm mới</button>
    </form>
@endsection
