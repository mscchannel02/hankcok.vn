@extends('admin.layouts.master')

@section('content')
    <div class="row mb-3">
        <div class="col-md-6">
            <a class="text-right about-sectiond-inline-block btn btn-success"
                href="{{ route('ad.khoachuyenmon.create') }}">Tạo mới</a>
        </div>
        <div class="col-md-6 text-right">
            <caption>
                <form class="w-100 d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                    <div class="input-group">
                        <input type="text" class="form-control small" placeholder="Search for..."
                            aria-label="Search" aria-describedby="basic-addon2" name="s">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">
                                <i class="fa fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </caption>
        </div>
    </div>
    <table class="table table-dark table-hover text-white">
        <thead>
            <tr>
                <th scope="col">Stt</th>
                <th scope="col">Name</th>
                <th scope="col">phone</th>
                <th scope="col">action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($khoachuyenmons as $key => $khoachuyenmon)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $khoachuyenmon->name }}</td>
                    <td>{{ $khoachuyenmon->phone }}</td>
                    <td>
                        <a href="{{ route('ad.khoa.edit', $khoachuyenmon) }}" class="btn btn-sm btn-success">
                            <i class="fa fa-edit"></i>
                        </a>

                        <form class="d-inline-block" action="{{ route('ad.khoa.destroy', $khoachuyenmon) }}" method="post" onsubmit="return confirm('Bạn có chắc chắn muốn xóa ?')">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-sm btn-danger" type="submit">
                                <i class="fa fa-trash "></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>
    {{-- <form action="" method="post" id="formDelete">
        @csrf
        @method('DELETE')
    </form> --}}
    <hr>
    {{-- {{ $khoas->links() }} --}}
@endsection


{{-- @push('js')
    <script>
        $('.btnDelete').click(function(e) {
            e.preventDefault();
            var _href = $(this).attr('href');
            // alert(href);
            $('#formDelete').attr('action', _href);
            
            if(confirm('Bạn có chắc muốn xóa không ?')) {
                $('#formDelete').submit();
            }
        });
    </script>
@endpush --}}

@stack('name')

@push('name')
    
@endpush
