@extends('admin.layouts.master')

@section('content')
    @if ($errors->any())
        <p class="alert alert-danger">
            <span>Vui lòng kiểm tra lại các trường</span>
        </p>
    @endif

    <a class="btn btn-sm btn-success" href="{{ route('ad.khoachuyenmon.index') }}">List Danh mục</a>
    <form method="post" action="{{ route('ad.khoachuyenmon.store') }}" class="text-white">
        @csrf
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">name</label>
                <input type="text" class="form-control" id="name" placeholder="name" name="name" value="{{old('name')}}">
                @error('name')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-group col-md-6">
                <label for="phone">phone</label>
                <input type="text" class="form-control" id="phone" placeholder="phone" name="phone" value="{{old('phone')}}">
                @error('phone')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>


        <button type="submit" class="btn btn-primary">Thêm mới</button>
    </form>
@endsection
