@extends('admin.layouts.master')

@section('content')
    @if ($errors->any())
        <p class="alert alert-danger">
            <span>Vui lòng kiểm tra lại các trường</span>
        </p>
    @endif

    <a class="btn btn-sm btn-success" href="{{ route('ad.blog.index') }}">List Danh sách</a>
    <form method="post" action="{{ route('ad.blog.store') }}" class="text-white" enctype="multipart/form-data">
        @csrf
        <div class="form-row row">
            <div class="form-group col-md-6">
                <label for="title">Tiêu đề</label>
                <input type="text" class="form-control" id="title" placeholder="title" name="title"
                    value="{{ old('title') }}">
                @error('title')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>


            <div class="form-group col-md-6">
                <label for="slug">slug</label>
                <input type="text" class="form-control" id="slug" placeholder="slug" name="slug"
                    value="{{ old('slug') }}">
                @error('slug')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="image">Image</label>
                <input type="file" class="form-control" placeholder="image" name="image" onchange="readURL(this);"
                    value="{{ old('image') }}">
                <img id="image" alt="your image" style="width:150px" />
                @error('image')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-group col-md-6">
                <label for="category_id">Danh mục <span class="required">*</span></label>
                <select name="category_id" class="form-control" id="category_id">
                    @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="description">description</label>
            <textarea name="description" cols="30" rows="10" id="description" placeholder="1234 Main St"
                class="form-control" {{old('description')}}></textarea>
        </div>

        <div class="form-group">
            <label for="content">content</label>
            <textarea name="content" cols="30" rows="10" id="content" placeholder="1234 Main St"
                class="form-control"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Thêm mới</button>
    </form>
    
@endsection

@push('css')
    <style>
        img{
  max-width:180px;
}
input[type=file]{
padding:10px;
background:#2d2d2d;}
    </style>
@endpush

@push('js')
    <script>
             function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endpush
