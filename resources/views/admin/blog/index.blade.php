@extends('admin.layouts.master')

@section('content')
    
<div class="row mb-3">
    <div class="col-md-6">
        <a class="btn btn-circle btn-sm btn-primary"
            href="{{ route('ad.blog.create') }}">Tạo mới</a>
    </div>
    <div class="col-md-6 text-right">
        <caption>
            <form class="w-100 d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                <div class="input-group">
                    <input type="text" class="form-control small" placeholder="Search for..."
                        aria-label="Search" aria-describedby="basic-addon2" name="search">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <i class="fa fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
            </form>
        </caption>
    </div>
</div>
<table class="table table-striped table-dark">
    <thead>
        <tr>
            <th scope="col">Stt</th>
            <th scope="col">Tiêu đề</th>
            {{-- <th scope="col">slug</th> --}}
            <th scope="col">image</th>
            <th scope="col">danh mục</th>
            <th scope="col">Mô tả ngắn</th>
            <th class="scope">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($blogs as $key => $blog)
            <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td >{{ $blog->title }}</td>
                {{-- <td>{{ $blog->slug }}</td> --}}
                <td>
                    <img src="{{ $blog->image }}" alt="your image" style="width:100px; height: 60px">
                </td>
                <td>{{ $blog->category->name }}</td>
                <td>
                    @if ($blog->description)
                        {!! $blog->description !!}
                    @else
                        Chưa có mô tả ngắn
                    @endif
                </td>
                <td>
                    <a href="{{ route('ad.blog.edit', $blog) }}" class="btn btn-sm btn-success">
                        <i class="fa fa-edit"></i>
                    </a>

                    <form class="d-inline-block" action="{{ route('ad.blog.destroy', $blog->id) }}" method="post" onsubmit="return confirm('Bạn có chắc chắn muốn xóa ?')">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-sm btn-danger" type="submit">
                            <i class="fa fa-trash "></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
{{$blogs->links()}}
@endsection