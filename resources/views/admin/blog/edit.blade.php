@extends('admin.layouts.master')

@section('content')
    @if ($errors->any())
        <p class="alert alert-danger">
            <span>Vui lòng kiểm tra lại các trường</span>
        </p>
    @endif

    <a class="btn btn-sm btn-success" href="{{ route('ad.blog.index') }}">List Danh sách</a>
    <form method="post" action="{{ route('ad.blog.update', $blog) }}" class="text-white" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-row row">
            <div class="form-group col-md-6">
                <label for="title">Tiêu đề</label>
                <input type="text" class="form-control" id="title" placeholder="title" name="title"
                    value="{{ $blog->title }}">
                @error('title')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>


            <div class="form-group col-md-6">
                <label for="slug">slug</label>
                <input type="text" class="form-control" id="slug" placeholder="slug" name="slug"
                    value="{{ $blog->slug }}">
                @error('slug')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="image">Image</label>
                <input type="file" class="form-control" placeholder="image" name="image" onchange="readURL(this);"
                    value="{{ $blog->image }}">
                <img id="image" src="{{ $blog->image }}" alt="your image" style="width:150px" />
                @error('image')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-group col-md-6">
                <label for="category_id">Danh mục <span class="required">*</span></label>
                <select name="category_id" class="form-control" id="category_id">
                    @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="content">content</label>
            <textarea name="content" cols="30" rows="10" id="content" placeholder="1234 Main St"
                class="form-control" value="{{$blog->content}}"></textarea>
        </div>

        <div class="form-actions">
            <button class="btn btn-primary btn-sm">Cập nhật</button>
            <a href="{{ route('ad.blog.index') }}" class="btn btn-sm btn-success">Quay lại</a>
        </div>
    </form>
    
@endsection

