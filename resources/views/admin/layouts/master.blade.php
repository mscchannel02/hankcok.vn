<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('backend/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('backend/img/favicon.png') }}">
    <title>
        Thuận 3 Tuổi
    </title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
    <!-- Nucleo Icons -->
    <link href="{{ asset('backend/css/nucleo-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('backend/css/nucleo-svg.css') }}" rel="stylesheet" />
    <link href="{{ asset('backend/css/main.css') }}" rel="stylesheet" />
    <link href="{{ asset('backend/venders/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" />
    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- CSS Files -->
    <link id="pagestyle" href="{{ asset('backend/css/material-dashboard.css?v=3.0.4') }}" rel="stylesheet" />
</head>

<body class="g-sidenav-show  bg-gray-200">

    <!-- Navbar header -->
    @include('admin.layouts.includes.sidebar')
    <!-- Navbar header -->


    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <!-- Navbar header -->
        @include('admin.layouts.includes.header')
        <!-- End Navbar -->
        <div class="container-fluid py-4">

            <!-- content -->
            @yield('content')
            <!-- End content -->

            <!-- footer -->
            @include('admin.layouts.includes.footer')
            <!-- End footer -->

        </div>
    </main>

    <!--  setting dashboard -->
    @include('admin.layouts.includes.setting')
    <!-- End dashboard -->

    <!--   Core JS Files   -->
    <script src="{{ asset('backend/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('backend/js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/smooth-scrollbar.min.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/chartjs.min.js') }}"></script>
    <script src="{{ asset('backend/js/main.js') }}"></script>
    

    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('backend/js/material-dashboard.min.js?v=3.0.4') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    @stack('js')

    @if (session('success'))
        <script>
            Swal.fire(
                'Good job!',
                '{{ session('success') }}',
                'success'
            )
        </script>
    @elseif(session('error'))
        <script>
            Swal.fire(
                icon: 'error',
                '{{ session('error') }}',
                'success'
            )
        </script>
    @endif

</body>

</html>
