@extends('admin.layouts.master')

@section('content')
    @if ($errors->any())
        <p class="alert alert-danger">
            <span>Vui lòng kiểm tra lại các trường</span>
        </p>
    @endif

    <a class="btn btn-sm btn-success" href="{{ route('ad.category.index') }}">List Danh sách</a>
    <form method="post" action="{{ route('ad.category.store') }}" class="text-white">
        @csrf
        <div class="form-row row">
            <div class="form-group col-md-6">
                <label for="name">name</label>
                <input type="text" class="form-control" id="name" placeholder="name" name="name" value="{{old('name')}}">
                @error('name')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            

            <div class="form-group col-md-6">
                <label for="slug">slug</label>
                <input type="text" class="form-control" id="slug" placeholder="slug" name="slug" value="{{old('slug')}}">
                @error('slug')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="form-group">
            <label for="description">description</label>
            <textarea name="description" cols="30" rows="10" id="description" placeholder="1234 Main St"
                class="form-control"></textarea>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="status">status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" selected>hiện</option>
                    <option value="0">ẩn</option>
                </select>
            </div>

            <div class="form-group col-md-6">
                <label for="prioty">Vị trí </label>
                <input type="text" name="prioty" id="prioty"  class="form-control" value="{{old('prioty')}}">
                @error('prioty')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <button type="submit" class="btn btn-sm btn-primary">Thêm mới</button>
    </form>
@endsection
