@extends('admin.layouts.master')

@section('content')
    <div class="row mb-3">
        <div class="col-md-6">
            <a class="btn btn-circle btn-sm btn-primary"
                href="{{ route('ad.category.create') }}">Tạo mới</a>
        </div>
        <div class="col-md-6 text-right">
            <caption>
                <form action="{{ route('ad.category.search') }}" id="idForm" class="w-100 d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                    @csrf
                    <div class="input-group">
                        <input id="seacrh" type="text" class="form-control" placeholder="Search for..."
                            aria-label="Search" aria-describedby="basic-addon2" name="search">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="">
                                <i class="fa fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </caption>
        </div>
    </div>
    <table class="table table-striped table-dark  ">
        <thead>
            <tr>
                <th scope="col">Stt</th>
                <th scope="col">Name</th>
                <th scope="col">slug</th>
                <th scope="col">description</th>
                <th scope="col">status</th>
                <th scope="col">prioty</th>
                <th scope="col">action</th>
            </tr>
        </thead>
        <tbody id="cate_body">
            @foreach ($categories as $key => $item)
                <tr >
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->slug }}</td>
                    <td>
                        @if ($item->description)
                            {!! $item->description !!}
                        @else
                            Chưa có mô tả ngắn
                        @endif
                    </td>
                    <td>
                        @if ($item->status == 1)
                            <span class="btn btn-sm btn-primary">Public</span>
                        @else
                            <span class="btn btn-sm btn-danger">private</span>
                        @endif

                    </td>
                    <td>{{ $item->prioty }}</td>
                    <td>
                        <a href="{{ route('ad.category.edit', $item) }}" class="btn btn-sm btn-success">
                            <i class="fa fa-edit"></i>
                        </a>
                        <form class="d-inline-block" action="{{ route('ad.category.destroy', $item) }}" method="post" onsubmit="return confirm('Bạn có chắc chắn muốn xóa ?')">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-sm btn-danger" type="submit">
                                <i class="fa fa-trash "></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>
    <hr>
    {{ $categories->links() }}
@endsection


{{-- 
@push('js')
    <script>
        // this is the id of the form
        $( "#seacrh" ).keyup(function() {
            // $("#idForm").submit(function(e) {
            // e.preventDefault(); 

            var form = $("#idForm");
            var actionUrl = form.attr('action');
            $.ajax({
                type: "GET",
                url: actionUrl,
                data: form.serialize(), // serializes the form's elements.
                success: function(data)
                {
                    var respone = JSON.parse(data);
                    var html = '';
                    var i = 1;
                    respone.data.forEach(function(item) {
                        console.log(item);
                        html += `<tr>\
                        <th>${i}</th>\
                        <td>${item.name}</td>\
                        <td>${item.slug}</td>\
                        <td>${item.description}</td>\
                        <td>${item.status = 1 ? '<span class="btn btn-sm btn-primary">Public</span>' : ' <span class="btn btn-sm btn-primary">Private</span>'}</td>\
                        <td>${item.prioty}</td>\
                        <td>
                            <a href="{{ route('ad.category.edit', $item) }}" class="btn btn-sm btn-success">
                                <i class="fa fa-edit"></i>
                            </a>
                            <form class="d-inline-block" action="{{ route('ad.category.destroy', $item) }}" method="post" onsubmit="return confirm('Bạn có chắc chắn muốn xóa ?')">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-sm btn-danger" type="submit">
                                    <i class="fa fa-trash "></i>
                                </button>
                            </form>
                        </td>\
                        </tr>`;
                        i++;
                    });
                    $('#cate_body').html(html);


                    console.log(respone); // show response from the php script.
                }
            });
        // });
        });
        
    </script>
@endpush --}}
