<?php

namespace App\Models;

use App\Models\Sinhvien;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Khoahoc extends Model
{
    use HasFactory;
    // protected $table = 'khoahocs';
    protected $fillable = [
        'name'
    ];

    public function sinhviens()
    {
        return $this->hasMany(Sinhvien::class);
    }

     
}
