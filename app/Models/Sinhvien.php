<?php

namespace App\Models;

use App\Models\Khoahoc;
use App\Models\Khoachuyenmon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Sinhvien extends Model
{
    use HasFactory;
    // protected $table = 'sinhviens';
    protected $fillable = [
    'name', 'gender', 'date', 'address', 'makhoacm_id', 'makhoahoc_id'
    ];

    public function khoachuyenmon()
    {
        return $this->belongsTo(Khoachuyenmon::class,'makhoacm_id');
    }

    public function khoahoc()
    {
        return $this->belongsTo(Khoahoc::class, 'makhoahoc_id');
    }
}
