<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Blog extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'slug', 'image', 'category_id', 'description', 'content'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    
}
