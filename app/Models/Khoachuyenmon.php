<?php

namespace App\Models;

use App\Models\Sinhvien;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Khoachuyenmon extends Model
{
    use HasFactory;
    // protected $table = 'khoachuyenmons';
    protected $fillable = [
        'name', 'phone'
    ];

    public function sinhviens()
    {
        return $this->hasMany(Sinhvien::class);
    }
}
