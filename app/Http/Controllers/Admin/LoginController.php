<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('admin.login');
    }

    // public function login(LoginRequest $request,Admin $admin)
    // {

    //     $email = $request->email;
    //     $password = $request->password;
    //     $user = $admin->where('email',$email)->first();
    //     $user_ss = $user->toArray();
    //     if ($user) {
    //         if(Hash::check($password, $user->password)){
    //             $data = 
    //             session(['admin' => $user_ss]);
    //             return redirect()->route('ad.dashboard');
    //         } else {
    //             return redirect()->back()->with('error','Email hoặc mật khẩu bị sai');
    //         }
    //     }

    // }

    // public function logout(Request $request)
    // {
    //     $request->session()->forget('admin');
    //     return redirect()->route('ad.login')->with('success', 'logout thành công');
    // }


    public function login(LoginRequest $request)
    {

        $login = $request->only('email', 'password');
        if (Auth::attempt($login)) {

            session()->flash('success', 'Đăng nhập thành công');
            return redirect()->route('ad.dashboard');
        } else {
            session()->flash('error', 'Email hoặc Mật khẩu không chính xác');
            return redirect()->route('ad.login.index');
        }
    }

    

    public function logout()
    {
        Auth::logout();
        return redirect()->route('ad.login')->with('success', 'logout thành công');
    }
}
