<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Khoachuyenmon;
use Illuminate\Http\Request;

class KhoachuyenmonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $khoachuyenmons = Khoachuyenmon::all();
        return view('admin.khoa.index', compact('khoachuyenmons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.khoa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $check = Khoachuyenmon::create($data);
        session()->flash('sucess','Thêm thành công');
        return redirect()->route('ad.khoachuyenmon.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Khoachuyenmon $khoa )
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Khoachuyenmon $khoa)
    {
        return view('admin.khoachuyenmon.edit', compact('khoachuyenmon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Khoachuyenmon $khoachuyenmon)
    {
        $data = $request->all();
        $khoachuyenmon->update($data);
        return redirect()->route('ad.khoachuyenmon.index')->with('success','Cập nhật thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Khoachuyenmon $khoachuyenmon)
    {
        $khoachuyenmon->delete();
        return redirect()->route('ad.khoachuyenmon.index')->with('success','Xóa thành công');
    }
}
