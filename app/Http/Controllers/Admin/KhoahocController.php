<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Khoahoc;
use Illuminate\Http\Request;

class KhoahocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $khoahocs = Khoahoc::all();
        return view('admin.khoahoc.index', compact('khoahocs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.khoahoc.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        Khoahoc::create($data);

        session()->flash('success', 'Thêm thành công');
        return redirect()->route('ad.khoahoc.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Khoahoc $khoahoc)
    {
        return view('admin.khoahoc.edit', compact('khoahoc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Khoahoc $khoahoc)
    {
        $data = $request->all();
        $khoahoc->update($data);
        return redirect()->route('ad.khoahoc.index')->with('success', 'Đã sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Khoahoc $khoahoc)
    {
        $khoahoc->delete();
        return redirect()->route('ad.khoahoc.index')->with('success', 'Đã xóa thành công');
    }
}
