<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateSinhvienRequest;
use App\Models\Khoachuyenmon;
use App\Models\Khoahoc;
use App\Models\Sinhvien;
use Illuminate\Http\Request;

class SinhvienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $sinhviens = Sinhvien::orWhere('name', 'like', '%' . $request->search . '%')->paginate(3)->withQueryString();

        $khoahocs = Khoahoc::all();
        $khoachuyenmons = Khoachuyenmon::all();
        return view('admin.sinhvien.index', compact('sinhviens', 'khoahocs', 'khoachuyenmons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $khoahocs = Khoahoc::all();
        $khoachuyenmons = Khoachuyenmon::all();
        return view('admin.sinhvien.create', compact('khoahocs', 'khoachuyenmons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSinhvienRequest $request)
    {
        $data = $request->all();
        $check = Sinhvien::create($data);
        if ($check) {
            return redirect()->route('ad.sinhvien.index')->with('success', 'thêm mới thành công');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sinhvien $sinhvien)
    {
        return view('admin.sinhvien.edit', compact('sinhvien'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sinhvien $sinhvien)
    {
        $data = $request->all();
        $check = $sinhvien->update($data);
        if ($check) {
            return redirect()->route('ad.sinhvien.index')->with('success', 'sửa thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sinhvien $sinhvien)
    {
        $sinhvien->delete();
        return redirect()->route('ad.sinhvien.index')->with('success', 'Xóa thành công');
    }


    
}
