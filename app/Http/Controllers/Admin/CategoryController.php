<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::orWhere('name', 'like', '%' . $request->search . '%')->paginate(7)->withQueryString();
        return view('admin.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        // $request->validate($rule, $messages)

        // $category = $request->all();
        $check = Category::create($request->all());
        if ($check) {
            session()->flash('success', 'Thêm thành công');
            return redirect()->route('ad.category.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        // $category = Category::find($id);
        return view('admin.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        // $category = Category::find($id);
        
        $check = $category->update($request->all());
        // $check = $category->save();
        if ($check) {
            session()->flash('success', 'Sửa thành công');
            return redirect()->route('ad.category.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        // $category = Category::find($id);
        $check = $category->delete();
        if ($check) {
            session()->flash('success', 'xóa thành công');
            return redirect()->route('ad.category.index');
        }
    }


    public function search(Request $request)
    {
        if ($request->search == '') {
            $categories = Category::all();
        }else {
            $categories = Category::orWhere('name', 'like' , '%' . $request->search . '%' )->paginate(7)->withQueryString();
        }

        return response( $categories, 200)
                  ->header('Content-Type', 'text/plain');
    }

}
