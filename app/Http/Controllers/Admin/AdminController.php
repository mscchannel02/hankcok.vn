<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\CreateAdminRequest;
use App\Http\Requests\UpdateAdminRequest;

class AdminController extends Controller
{
    
    public function index()
    {
        $admins = Admin::orderBy('id', 'DESC')->paginate(3);
        return view('admin.admins.index', compact('admins'));
    }
    
    public function create()
    {
        return view('admin.admins.create');
    }
    
    public function store(CreateAdminRequest $request)
    {
        $data = $request->only('name', 'email', 'phone');
        $data['password'] = Hash::make($request->password);

        $check = Admin::create($data);
        session()->flash('success', 'Thành công');
        if ($check) {
            return redirect()->route('ad.admin.index');
        }
        
    }
    
    public function edit(Admin $admin)
    {
        return view('admin.admins.edit', compact('admin'));
    }
    
    public function update(UpdateAdminRequest $request, Admin $admin)
    {
        $data = $request->only('name', 'phone');
        $data['password'] = Hash::make($request->password);
        if ($request->password) {
            
            $request->validate([
                'password' => 'required|min:6|max:15'
            ]);

            $data['password'] = Hash::make($request->password);
        }
        
        $check = $admin->update($data);
        session()->flash('success', 'Thành công');
        if ($check) {
            return redirect()->route('ad.admin.index');
        }
        
    }

    public function destroy(Admin $admin)
    {
        $admin->delete();
        session()->flash('success', 'Thành công');

        return redirect()->route('ad.admin.index');
    }


}
